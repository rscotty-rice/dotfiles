#!/bin/sh


if [ $# == 1 ]; then
	if [ $1 == "install" ]; then
		INSTALL_OPERATION=0
	elif [ $1 == "git-install" ]; then
		export GIT_BASE_DIR=$(realpath ${0%/*})
		INSTALL_OPERATION=1
	fi
else
	echo "Invalid Arguments: $0 <[git-]install>"
	exit
fi


export SLSKD_VERSION="0.16.24"

curl -L https://github.com/slskd/slskd/releases/download/$SLSKD_VERSION/slskd-$SLSKD_VERSION-linux-x64.zip > /tmp/.slskd-$SLSKD_VERSION.zip

if [ $INSTALL_OPERATION == 1 ]; then
	mkdir $GIT_BASE_DIR/home/rscott/.local/share/slskd
	unzip /tmp/.slskd-$SLSKD_VERSION.zip -d $GIT_BASE_DIR/home/rscott/.local/share/slskd
fi

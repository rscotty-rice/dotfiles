#!/bin/sh

# Wait for internet connection
while [ 1 ]; do
  if [ $(ip add sh dev enp5s0 | grep inet | wc -l) -ne 0 ]; then
     break
  fi
  sleep 1
done

curl $(python ~/.local/share/nat-geo-wallpaper/main.py) -o /tmp/.current-natgeo
swaybg -m fill -i /tmp/.current-natgeo &

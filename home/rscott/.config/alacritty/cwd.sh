#/bin/sh
readarray -t window_info <<< $(hyprctl -j activewindow | jq 'select(.Class,.pid?) | .class,.pid')

if [[ ${window_info[0]} == "\"Alacritty\"" ]]; then
	ppid=$(pgrep --newest --parent ${window_info[1]})
	/usr/bin/alacritty --working-directory $(readlink /proc/${ppid}/cwd) $@ &
else
	/usr/bin/alacritty $@ &
fi

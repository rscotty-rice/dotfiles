#!/bin/sh
#
# [ MPD-Auto Script ]
# Automatically download album art and play music using mpc(1).
# Author: Reece Scott (personal@rscott.xyz)
#

mpc="mpc --host $(cat ~/.local/share/mpd-passwd)@192.168.1.19"
mpd_stream="http://192.168.1.19:8000/"
albumart_path="/tmp/.albumart"
albumart_placeholder="/home/rscott/.config/mpd-cover-placeholder"

cp $albumart_placeholder $albumart_path

$mpc idleloop | while read mpc_idle;
do
	if [ $mpc_idle = "player" ]; then
		mpc_current_track=$(${mpc} current)

		if [ -z "$mpc_current_track" ]; then
			# Stopped
			[ -z $mpv_pid ] && kill $mpv_pid

			cp $albumart_placeholder $albumart_path
		else
			# Playing/Paused

			if [ -z $mpv_pid ] || ! kill -0 $mpv_pid ; then
				mpv $mpd_stream &
				mpv_pid=$!
			fi

			if [ "$($mpc status "%state%")" = "playing" ]; then
				$mpc albumart "$($mpc status -f %file% | head -n 1)" > /tmp/.albumart || \
				$mpc readpicture "$($mpc status -f %file% | head -n 1)" > /tmp/.albumart || \
				cp $albumart_placeholder $albumart_path
			else
				cp $albumart_placeholder $albumart_path
			fi
		fi
	fi
done

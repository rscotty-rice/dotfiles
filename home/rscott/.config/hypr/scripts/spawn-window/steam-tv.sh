#!/bin/sh
#
# [ Steam TV Shell Script ]
# Launches steam with -steamdeck, closes steam elsewhere and sets appropriate resolution.
# Author: Reece Scott (personal@rscott.xyz)
#

# TODO Check if steam is open in -gamepadui.
killall -0 gamescope-session && /usr/bin/hyprctl dispatch workspace 10 && exit

kill -0 $(cat $HOME/.steampid) &&	killall -9 steam

# Size of the screen. If not set gamescope will detect native resolution from drm.
export SCREEN_HEIGHT=1080
export SCREEN_WIDTH=1920

# Allow power management. TODO: Peripheral devices might not halt and will suspend mid-game
export SDL_VIDEO_ALLOW_SCREENSAVER=1

# Override entire Steam client command line
export STEAMCMD="/usr/bin/steam -gamepadui -steamos3 -steampal -steamdeck -pipewire-dmabuf"

# Override the entire Gamescope command line
# This will not use screen and render sizes above
# GAMESCOPECMD="gamescope -e -f"

/usr/bin/hyprctl dispatch workspace 10

/usr/bin/gamescope-session

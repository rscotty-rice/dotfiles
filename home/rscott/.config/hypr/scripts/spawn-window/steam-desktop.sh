#!/bin/sh
#
# [ Steam Desktop Shell Script ]
# Launches steam with -steamdeck, closes steam elsewhere and launches steam
# whilst retaining '-steamdeck' to avoid unnecessary updates.
# Author: Reece Scott (personal@rscott.xyz)
#

# TODO Check if steam is open in gamepadui
kill -0 $(cat $HOME/.steampid) &&	killall -9 steam

/usr/bin/steam -steamdeck $@

call plug#begin()

Plug 'airblade/vim-gitgutter'
Plug 'itchyny/lightline.vim'
Plug 'myusuf3/numbers.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'ntpeters/vim-better-whitespace'
Plug 'sheerun/vim-polyglot'

call plug#end()

"
" [ Color Scheme ]
"

colorscheme noctu

let g:lightline = {
      \ 'colorscheme': 'base16_terminal',
      \ }

" Yank to clipboard
set clipboard+=unnamedplus

"
" [ Misc ]
"

" Bindings
" Clear highlight with ESC
map <esc> :noh<CR>

" Next / Prev tab with J & K
nmap J :tabp<CR>
nmap K :tabn<CR>

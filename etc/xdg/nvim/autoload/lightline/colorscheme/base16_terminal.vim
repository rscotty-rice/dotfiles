" base16 lightline - Vim lightline color scheme for 16-color terminals
" w/ modifications made by rscotty
" --------------------------------------------------------------
" Forked From: s417-lama (https://github.com/s417-lama/base16-terminal-lightline.vim)
" --------------------------------------------------------------

let s:black        = 0
let s:darkred      = 1
let s:darkgreen    = 2
let s:darkyellow   = 3
let s:darkblue     = 4
let s:darkmagenta  = 5
let s:darkcyan     = 6
let s:lightgrey    = 7
let s:darkgrey     = 8
let s:lightred     = 9
let s:lightgreen   = 10
let s:lightyellow  = 11
let s:lightblue    = 12
let s:lightmagenta = 13
let s:lightcyan    = 14
let s:white        = 15

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}
let s:p.normal.left    = [[s:black, s:darkblue, 'bold'], [s:darkblue, s:black]]
let s:p.normal.middle  = [[s:black, s:black]]
let s:p.normal.right   = [[s:black, s:darkblue], [s:darkblue, s:black], [s:darkblue, s:black]]
let s:p.inactive.left  = [[s:darkgrey, s:black], [s:black, s:black]]
let s:p.inactive.right = [[s:black, s:black], [s:black, s:black], [s:black, s:black]]
let s:p.insert.left    = [[s:black, s:darkgreen  , 'bold'], [s:darkgreen  , s:black]]
let s:p.insert.right   = [[s:black, s:darkgreen], [s:darkgreen, s:black], [s:darkgreen, s:black]]
let s:p.visual.left    = [[s:black, s:darkyellow, 'bold'], [s:darkyellow, s:black]]
let s:p.visual.right   = [[s:black, s:darkyellow], [s:darkyellow, s:black], [s:darkyellow, s:black]]
let s:p.replace.left   = [[s:black, s:darkred   , 'bold'], [s:darkred   , s:black]]
let s:p.replace.right  = [[s:black, s:darkred], [s:darkred, s:black], [s:darkred, s:black]]
let s:p.tabline.left   = [[s:darkblue, s:black]]
let s:p.tabline.tabsel = [[s:black, s:darkblue, 'bold']]
let s:p.tabline.middle = [[s:black, s:black]]
let s:p.tabline.right  = [[s:darkblue, s:black]]
let s:p.normal.error   = [[s:black, s:darkred]]
let s:p.normal.warning = [[s:black, s:darkyellow]]

let g:lightline#colorscheme#base16_terminal#palette = lightline#colorscheme#fill(s:p)

#!/bin/sh

export PATH="$HOME/.local/bin:$PATH"

export QT_QPA_PLATFORMTHEME=qt5ct

export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"

alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"
alias vim="nvim"
alias ssh='TERM=xterm-256color ssh'

# Getting applications to follow XDG Specification.
export ZDOTDIR="$XDG_CONFIG_HOME/zsh/"
export HISTFILE="$XDG_CONFIG_HOME/zsh/.zsh_history"
export KODI_DATA="$XDG_DATA_HOME"/kodi
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export HISTFILE="$XDG_STATE_HOME"/zsh/.zsh_history
export WINEPREFIX="$XDG_DATA_HOME"/wine-prefix
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
